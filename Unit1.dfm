object Form1: TForm1
  Left = 269
  Top = 195
  Width = 775
  Height = 449
  Caption = 'SuDoKu'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 360
    Top = 0
    Width = 109
    Height = 13
    Caption = 'Solutions. Click to view'
  end
  object Button1: TButton
    Left = 16
    Top = 360
    Width = 75
    Height = 25
    Caption = 'Find solutiions'
    TabOrder = 0
    OnClick = Button1Click
  end
  object ListBox1: TListBox
    Left = 360
    Top = 16
    Width = 313
    Height = 369
    ItemHeight = 13
    TabOrder = 1
    OnClick = ListBox1Click
  end
  object Button5: TButton
    Left = 176
    Top = 360
    Width = 75
    Height = 25
    Caption = 'Clear'
    TabOrder = 2
    OnClick = Button5Click
  end
  object od1: TOpenDialog
    FileName = '*.sko'
    Filter = '*.sko'
    Left = 392
    Top = 24
  end
  object MainMenu1: TMainMenu
    Left = 304
    Top = 16
    object File1: TMenuItem
      Caption = 'File'
      object New1: TMenuItem
        Caption = 'New'
        OnClick = New1Click
      end
      object Open1: TMenuItem
        Caption = 'Open'
        OnClick = Open3Click
      end
      object Save1: TMenuItem
        Caption = 'Save'
        OnClick = Save1Click
      end
      object Saveas1: TMenuItem
        Caption = 'Save as'
        OnClick = Saveas1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Caption = 'Exit'
        OnClick = Exit1Click
      end
    end
  end
  object sd1: TSaveDialog
    FileName = '*.sko'
    Filter = '*.sko'
    Left = 16
    Top = 8
  end
end

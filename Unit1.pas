unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Menus;

const maxfind = 20;

type
 tref = array [1..9] of ^integer;
  trref = array [1..9*9] of ^integer;

type
  TForm1 = class(TForm)
    Button1: TButton;
    od1: TOpenDialog;
    ListBox1: TListBox;
    Button5: TButton;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Open1: TMenuItem;
    Save1: TMenuItem;
    Saveas1: TMenuItem;
    N1: TMenuItem;
    Exit1: TMenuItem;
    Label1: TLabel;
    sd1: TSaveDialog;
    New1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    function validboard : boolean;
    procedure Edit1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Button1Click(Sender: TObject);
    function Solve(fields : trref; level, maxlevel : integer) : boolean;
    procedure DoShow(Sender: TObject);
    procedure Open3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    function BoardToString : string;
    procedure ListBox1Click(Sender: TObject);
    procedure SetBoard(s : string);
    procedure Button5Click(Sender: TObject);
    function  IsValidMove(i,j : integer): boolean;
    procedure Save1Click(Sender: TObject);
    procedure Saveas1Click(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure New1Click(Sender: TObject);
    procedure Edit1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    stopped, showing : boolean;
    thefilename : string;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


var board : array [1..9, 1..9] of integer;
    boxes, rows, cols : array [1..9] of tref;


function IsOK(ref : tref): boolean;
var i : integer;
    digits : set of 1..9;
begin
   result := true;
   digits := [1,2,3,4,5,6,7,8,9];
   for i := 1 to 9 do
   if ref[i]^ <> 0 then
   begin
     if not(ref[i]^ in digits) then
     begin
       result := false;
       exit;
     end;
     digits := digits - [ref[i]^];
   end;
end;

var edit : array [1..9, 1..9] of tedit;


procedure TForm1.FormCreate(Sender: TObject);
var i,j, i1, j1, n,m, dx, dy : integer;
begin
   m := 0;  dx := 0; dy := 0;
   for i := 1 to 3 do
   for j := 1 to 3 do
   begin
     inc(m); n:=0;
     for i1 := (i-1)*3+1 to (i-1)*3+3 do
     for j1 := (j-1)*3+1 to (j-1)*3+3 do
     begin
       inc(n);
       boxes[m,n] := @board[i1,j1];
     end;
   end;

   for i := 1 to 9 do
   for j := 1 to 9 do
   begin
     rows[j,i] := @board[i,j];
   end;

   for i := 1 to 9 do
   for j := 1 to 9 do
   begin
     cols[i,j] := @board[i,j];
   end;

     dx := 0; dy := 0;
   for i := 1 to 9  do
   begin
        if (i-1) mod 3 = 0 then inc(dx, 5);
        dy := 0;
   for j := 1 to 9  do
   begin
     edit [i,j] := tedit.create(form1);
     with edit[i,j] do
     begin
        parent := form1;
        if (j-1) mod 3 = 0 then inc(dy,10);
        left :=  i*30+dx;
        top :=   j*30+dy;
        height := 28;
        width := 28;
        name := 'ED'+inttostr(i)+inttostr(j);
        text := '';
        font.Size := 14;
        tag := integer(@board[i,j]);
        maxlength := 1;
        color := clyellow;
        onchange := edit1Change;
        onmousemove := edit1MouseMove;
        onDblClick  := edit1DblClick;
     end;
   end;
   end;
end;

procedure TForm1.Edit1Change(Sender: TObject);
var p : ^integer;
begin
   if showing then exit;
   with (sender as tedit) do
   begin
     color := clwhite;
     p := pointer((sender as tedit).tag);
     p ^ := strtointdef((sender as tedit).text,0);
     if p^>0 then
     color := clwhite else
     color := clYellow;

     if not validboard then
     begin
       (sender as tedit).Text := '';
       caption := 'Error';
     end else caption := 'OK';
   end;
end;
                                     


function TForm1.validboard: boolean;
var i : integer;
begin
  result := false;
  for i := 1 to 9 do
  if not isok(boxes[i]) then exit;

  for i := 1 to 9 do
  if not isok(rows[i]) then exit;

  for i := 1 to 9 do
  if not isok(cols[i]) then exit;
  result := true;
end;

function TForm1.IsValidMove(i,j : integer): boolean;
var i1, j1, n : integer;
begin
  result := false;
  i1 := (i-1) div 3 +1;
  j1 := (j-1) div 3 +1;
  n :=  (i1-1)*3 +j1;
  if not isok(boxes[n]) then exit;
  if not isok(rows[j]) then exit;
  if not isok(cols[i]) then exit;
  result := true;
end;

procedure TForm1.Edit1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
  var i : integer;
  p : ^integer;
  var c : string;
begin
 // exit;

  with sender as tedit do
  begin
     c := '';
     caption := '';
     //lb.caption := name;

     p := pointer(tag);
     if text = '' then
     begin
       x := strtoint(copy(name, 3,1));
       y := strtoint(copy(name, 4,1));
       for i := 1 to 9 do
       begin
         p^ := i;
         if validboard then begin caption := caption + c + inttostr(i); c := ','; end;
         //if IsValidMove(x,y) then begin lb.caption := lb.caption + c + inttostr(i); c := ','; end;
       end;
       p^ := 0;
     end;
  end;
end;

var fields : trref;
    remaining : integer;
    Location : array [1..81] of record x,y : integer; end;

procedure TForm1.Button1Click(Sender: TObject);
var    i,j,n : integer;
   s : string;
begin
try
  n := 0;
  button1.Enabled := false;
  Screen.Cursor := crHourGlass;
  stopped := false;
  listbox1.Items.clear;
  for i := 1 to 9 do
  for j := 1 to 9 do
  if board[i,j] = 0 then
  begin
    inc(n);
    fields[n] := @board[i,j];
    Location[n].x := i;
    Location[n].y := j;
  end;
  remaining := n;

  solve(fields, 1,remaining );

  if listbox1.items.count > MaxFind then
  s :=  'More than '+inttostr(maxfind)
  else s := inttostr(listbox1.items.count);
  caption := s+'  solution(s) found';

finally
  Screen.Cursor := crDefault;
  button1.Enabled := true;
end;

end;

function TForm1.Solve(fields: trref; level, maxlevel: integer): boolean;
var i,l : integer;
begin
  if level > maxlevel then
  begin
    if Listbox1.items.Count <= Maxfind then
    Listbox1.items.add(boardtostring) else Stopped := true;
  end
  else
  begin
    result := false;
    if not stopped then
    for i := 1 to 9 do
    begin
      fields[level]^ := i;
      if IsValidMove(Location[level].x, Location[level].y)  then
      result := solve(fields, level+1, maxlevel);
    end;
    fields[level]^ := 0;
  end;

end;

procedure TForm1.DoShow(Sender: TObject);
var i,j : integer;
begin
try
 showing := true;
 for i := 1 to 9 do
  for j := 1 to 9 do
  begin
    if (board[i,j] <=  0) then
    edit[i,j].Color := clYellow else
    edit[i,j].Color := clWhite;
    edit[i,j].Text :=
    inttostr(abs(board[i,j]));
    //board[i,j] := abs(board[i,j]);
    if board[i,j]= 0 then edit[i,j].text := '';
  end;
 finally
   showing := false;
 end;
end;

procedure TForm1.Open3Click(Sender: TObject);
var strl : tstringlist;
  i,j,n : integer;
begin
  strl := tstringlist.create;
  with od1 do
  if execute then
  begin
  n := 0;
  strl.loadfromfile(filename);
  for i := 1 to 9 do
  for j := 1 to 9 do
  begin
    inc(n);
    board[i,j] := strtoint(strl[0][n]);
  end;
  doShow(nil);
  end;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  if validboard then
  caption := 'OK' else
  caption := 'Not OK';
end;

function TForm1.BoardToString: string;
var i,j,n : integer;
begin
  n :=0;
  setlength(result, 81);
  for i := 1 to 9 do
  for j := 1 to 9 do
  begin
    inc(n);
    result[n] := inttostr(board[i,j])[1];
  end;

end;

procedure TForm1.ListBox1Click(Sender: TObject);
begin
  with listbox1 do
  SetBoard(items[itemindex]);

end;

procedure TForm1.SetBoard(s: string);
var i,j,n : integer;
begin
  n := 0;
  for i := 1 to 9 do
  for j := 1 to 9 do
  begin
    inc(n);
    if board[i,j] <= 0 then
    board[i,j] := -strtoint(s[n]) else
    board[i,j] := strtoint(s[n]);
  end;
  Doshow(nil);

end;

procedure TForm1.Button5Click(Sender: TObject);
 var i,j,n : integer;
begin
  n := 0;
  for i := 1 to remaining do
  fields[i]^:= 0;
  Doshow(nil);

end;

procedure TForm1.Save1Click(Sender: TObject);
var strl : tstringlist;
begin
  if theFilename = '' then Saveas1click(nil) else
  begin
  strl := tstringlist.Create;
  strl.Text := BoardToString;
  strl.savetofile(thefilename);
  end;
end;

procedure TForm1.Saveas1Click(Sender: TObject);
var strl : tstringlist;
begin
  //strl := tstringlist.Create;
  //strl.Text := BoardToString;
  with sd1 do
  if execute then
  begin
    thefilename := filename;
    save1click(nil);
  end;

end;

procedure TForm1.Exit1Click(Sender: TObject);
begin
  close;
end;

procedure TForm1.New1Click(Sender: TObject);
begin
  thefilename :='';
  fillchar(board, sizeof(board), #0);
  doShow(nil);
end;

procedure TForm1.Edit1DblClick(Sender: TObject);
begin
  (sender as tedit).Text := '';
end;

end.
